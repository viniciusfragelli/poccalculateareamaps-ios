//
//  MapViewController.swift
//  POCArea
//
//  Created by Vinicius Senff on 09/10/18.
//  Copyright © 2018 Livetouch. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    
    private var listaPontos: [CLLocationCoordinate2D] = []
    private var tempMarker: GMSMarker? = nil
    private var isAddPolygon: Bool = false
    private var polygon: GMSPolygon? = nil
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var txtArea: UILabel!
    @IBOutlet weak var viewExcluir: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: -25.445997587450147, longitude: -49.287230148911476, zoom: 18.0)
        self.map.camera = camera
        self.map.delegate = self
    }
    
    override func loadView() {
        super.loadView()
    }
    
    func drawPolygon(){
        if(listaPontos.count >= 3){
            if(isAddPolygon && polygon != nil){
//                polygon.remove();
            }
            let path = GMSMutablePath()
            listaPontos = OrdernarVetices.ordenarVertices(listaPontos)
            for loc in listaPontos {
                path.add(loc)
            }
            polygon = GMSPolygon(path: path)
            polygon?.fillColor = UIColor(red: 129/255, green: 199/255, blue: 132/255, alpha: 0.5)
            polygon?.strokeColor = UIColor(red: 57/255, green: 142/255, blue: 60/255, alpha: 1)
            polygon?.strokeWidth = 8
            polygon?.map = map
        }
        txtArea.text = "Área: \(SphericalUtil.computeArea(path: listaPontos)) m²"
    }
    
    func drawMarkers(){
        var i:Int = 1
        for p in listaPontos {
            let marker = GMSMarker()
            marker.position = p
            marker.title = "Ponto \(i)"
            marker.map = map
        }
    }
    
    func replacePosition(loc: CLLocationCoordinate2D){
        for i in 0...listaPontos.count-1  {
            if listaPontos[i].latitude == tempMarker?.position.latitude && listaPontos[i].longitude == tempMarker?.position.longitude {
                listaPontos.remove(at: i)
                listaPontos.append(loc)
                tempMarker = nil
                break
            }
        }
    }

    @IBAction func onClickExcluir(_ sender: UIButton) {
        for i in 0...listaPontos.count-1  {
            if listaPontos[i].latitude == tempMarker?.position.latitude && listaPontos[i].longitude == tempMarker?.position.longitude {
                listaPontos.remove(at: i)
                tempMarker = nil
                break
            }
        }
        UIView.animate(withDuration: 1, animations: {
            self.viewExcluir.isHidden = true
        })
        map.clear()
        drawPolygon()
        drawMarkers()
    }
}

extension MapViewController : GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if let mark = tempMarker {
            replacePosition(loc: coordinate)
        }else{
            listaPontos.append(coordinate)
        }
        UIView.animate(withDuration: 1, animations: {
            self.viewExcluir.isHidden = true
        })
        mapView.clear()
        drawPolygon()
        drawMarkers()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        tempMarker = marker
        UIView.animate(withDuration: 1, animations: {
            self.viewExcluir.isHidden = false
        })
        return false
    }
}
