//
//  OrdenarVetor.swift
//  POCArea
//
//  Created by Vinicius Senff on 09/10/18.
//  Copyright © 2018 Livetouch. All rights reserved.
//

import Foundation
import GoogleMaps

class OrdernarVetices {
    static func ordenarVertices(_ l: [CLLocationCoordinate2D]) -> [CLLocationCoordinate2D] {
        let center = findCenter(list: l)
        var lista = l
        lista.sort(by: {a,b in
            let a1:Double = ((atan2(a.latitude - center.latitude, a.longitude - center.longitude).toDegress()) + 360).remainder(dividingBy: 360);
            let a2:Double = ((atan2(b.latitude - center.latitude, b.longitude - center.longitude).toDegress()) + 360).remainder(dividingBy: 360);
            return (a1 < a2)
        })
        return lista
    }
    private static func findCenter(list: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        var x: Double = 0;
        var y: Double = 0;
        for p in list {
            x = x + p.latitude;
            y = y + p.longitude;
        }
        let newlat: Double = x/Double(list.count)
        let newlog: Double = y/Double(list.count)
        return CLLocationCoordinate2D(latitude: newlat, longitude: newlog);
    }
}

extension Double {
    func toDegress() -> Double {
        return self * 180 / 3.1415926
    }
}
